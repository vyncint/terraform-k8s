terraform {
    required_providers {
        azurerm = {
            source  = "hashicorp/azurerm"
            version = "=2.46.0"
        }
    }
}
variable "subscription_id" {
    default = "quickapp"
}
variable "client_id" {
    default = "West US"
}
variable "client_secret" {
    default = "quickapp"
}
variable "tenant_id" {
    default = "quickapp"
}
provider "azurerm" {
  features {}

  subscription_id = var.subscription_id
  client_id       = var.client_id
  client_secret   = var.client_secret
  tenant_id       = var.tenant_id
}

variable "prefix" {
    default = "quickapp"
}
variable "location" {
    default = "West US"
}
variable "dnsprefix" {
    default = "quickapp"
}
resource "azurerm_resource_group" "k8s" {
    name = "${var.prefix}-resource"
    location = "${var.location}"
}

resource "azurerm_kubernetes_cluster" "k8s" {
    name = "${var.prefix}-cluster"
    location = azurerm_resource_group.k8s.location
    resource_group_name = azurerm_resource_group.k8s.name
    node_resource_group = "${var.prefix}-node-resource"
    dns_prefix = "${var.dnsprefix}"
    default_node_pool {
        name = "default"
        node_count = 1
        vm_size = "Standard_B2s"
    }
    identity {
        type = "SystemAssigned"
    }
    tags = {
        Environment = "Production"
    }
}
resource "azurerm_public_ip" "k8s" {
    name = "${replace(lower(var.location), " ", "")}-${var.prefix}-test"
    location = azurerm_resource_group.k8s.location
    resource_group_name = azurerm_kubernetes_cluster.k8s.node_resource_group
    allocation_method = "Static"
    idle_timeout_in_minutes = 30
    ip_version = "IPv4"
    domain_name_label = "${var.prefix}-k8s"
    sku = "Standard"
}
output "client_certificate" {
    value = azurerm_kubernetes_cluster.k8s.kube_config.0.client_certificate
}
output "kube_config" {
    value = azurerm_kubernetes_cluster.k8s.kube_config_raw
    sensitive = true
}
output "static_ip" {
    value = azurerm_public_ip.k8s.ip_address
}
output "dns_label" {
    value = azurerm_public_ip.k8s.domain_name_label
}
output "full_dns" {
    value = azurerm_public_ip.k8s.fqdn
}
output "resource-name" {
    value = azurerm_resource_group.k8s.name
}
output "cluster-name" {
    value = azurerm_kubernetes_cluster.k8s.name
}